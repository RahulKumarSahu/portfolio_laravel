@extends('layouts.dashboard')

@section('title')
    Users
@endsection

@section('css')
    <link href="{{ asset('asset/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-11">
            <h2>Experiences</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Home</a>
                </li>
                <li class="active">
                    <a href="{{ route('experiences.index') }}">
                        <strong>Experiences </strong>
                    </a>
                </li>
            </ol>
        </div>
        <div class="col-lg-1">
            <a href="{{ route('experiences.create') }}" class="btn btn-primary mt-3">
                <span class="fa fa-plus"></span>
                Add </a>
        </div>
    </div>
@endsection

@section('content')

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Users List
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover users-table">
                                <thead>
                                    <tr>
                                        <th>Subject</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($experiences as $item)
                                        <tr>
                                            <td>{{ $item->title }}</td>
                                            <td> {{ $item->description }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('experiences.show', $item->id) }}"
                                                        class="btn-white btn btn-xs">View</a>

                                                    <a href="{{ route('experiences.edit', $item->id) }}"
                                                        class="btn-white btn btn-xs">Edit</a>

                                                    <a href="javascript:;" data-item-id="{{ $item->id }}"
                                                        class="btn-white btn btn-xs delete-item">Delete</a>

                                                        {{-- <form action="{{ route('experiences.destroy', $item->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                            <button type="submit" class="btn-white btn btn-xs">Delete</button>
                                                        </form> --}}
                                                </div>
                                            </td>
                                         </tr>
                                    @endforeach
                                </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('script')
    {{-- Datatable --}}
    <script src=" {{ asset('asset/js/plugins/dataTables/datatables.min.js') }}"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {
            $('.users-table').DataTable();

            @if (session('success'))
                toastr.success('{{ session('success') }}', 'Success');
            @endif

            $('.delete-item').click(function() {

                let itemId = $(this).data('item-id');

                // Generate Token method 1
                // let token = $("meta[name='csrf-token']").attr("content");

                // Generate Token method 2
                let token = '{{ csrf_token() }}';

                let url = '{{ route('experiences.destroy', ':id') }}';
                url = url.replace(':id', itemId);

                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function() {

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            "_token": token,
                            "_method": 'DELETE',

                        },
                        success: function() {
                            swal("Deleted!", "item account has been deleted.",
                                "success");
                            setTimeout(() => {
                                location.reload();
                            }, 3000);
                        }
                    });

                });
            });

        });
    </script>
@endsection

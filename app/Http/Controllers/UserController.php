<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\StoreUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function index()
    {
        Session::put('locale', 'en');
        app()->setlocale(Session::get('locale'));

        $users = User::get();

        return view('dashboard.users.index', compact('users'));
    }

    public function create()
    {
        return view('dashboard.users.create');
    }

    public function store(StoreUser $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->dob = $request->dob;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->contact_no = $request->contact_no;
        $user->father_name = $request->father_name;
        $user->mother_name = $request->mother_name;
        $user->marital_status = $request->marital_status;
        $user->spouse_name = $request->spouse_name;
        $user->anniversary = $request->anniversary;
        $user->bank_name = $request->bank_name;
        $user->bank_branch = $request->bank_branch;
        $user->account_number = $request->account_number;
        $user->ifsc_code = $request->ifsc_code;
        $user->save();

        return redirect('/users')->with(['success' => 'User has been created.']);
    }

    public function show($id)
    {
        // dd($id);
        $user = User::find($id);
        return view('dashboard.users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = User::find($id);

        return view('dashboard.users.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $update = User::find($id);
        $update->name = $request->name;
        $update->gender = $request->gender;
        $update->dob = $request->dob;
        $update->email = $request->email;
        $update->password = Hash::make($request->password);
        $update->contact_no = $request->contact_no;
        $update->father_name = $request->father_name;
        $update->mother_name = $request->mother_name;
        $update->marital_status = $request->marital_status;
        $update->spouse_name = $request->spouse_name;
        $update->anniversary = $request->anniversary;
        $update->bank_name = $request->bank_name;
        $update->bank_branch = $request->bank_branch;
        $update->account_number = $request->account_number;
        $update->ifsc_code = $request->ifsc_code;
        $update->save();

        return redirect('/users')->with(['success' => 'User has been updated.']);
    }

    public function destroy($id)
    {
        User::find($id)->delete();

        return redirect('/users')->with(['success' => 'User has been deleted.']);
    }
}
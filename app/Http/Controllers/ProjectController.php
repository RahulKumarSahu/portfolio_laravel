<?php

namespace App\Http\Controllers;

use App\Http\Requests\Projects\StoreProject;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::get();
        return view('dashboard.project.index', compact('projects'));
    }

    public function create()
    {
        return view('dashboard.project.create');
    }

    public function store(StoreProject $request)
    {
        $project = new Project;
        $project->title = $request->title;
        $project->visit = $request->visit;
        $project->gitlab = $request->gitlab;
        $project->description = $request->description;
        $project->status = $request->status;
        $project->save();

        return redirect('/projects')->with(['success' => 'Project has been created.']);
    }

    public function show($id)
    {
        $project = Project::find($id);
        return view('dashboard.project.show', compact('project'));
    }

    public function edit($id)
    {
        $project = Project::find($id);
        return view('dashboard.project.edit', compact('project'));
    }


    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        $project->title = $request->title;
        $project->visit = $request->visit;
        $project->gitlab = $request->gitlab;
        $project->description = $request->description;
        $project->status = $request->status;
        $project->save();

        return redirect('/projects')->with(['success' => 'Project has been updated.']);
    }

    public function destroy($id)
    {
        Project::find($id)->delete();

        return redirect('/projects')->with(['success' => 'Project has been deleted.']);
    }
}
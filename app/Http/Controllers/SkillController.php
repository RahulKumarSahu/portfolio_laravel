<?php

namespace App\Http\Controllers;

use App\Models\Skill;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    public function index()
    {
        $skills = Skill::get();
        return view('dashboard.skill.index', compact('skills'));
    }

    public function create()
    {
        return view('dashboard.skill.create');
    }

    public function store(Request $request)
    {
        $skill = new Skill;
        $skill->name = $request->name;
        $skill->status = $request->status;
        $skill->save();
        return redirect('/skills')->with(['success' => 'Project has been Created.']);
    }

    public function show($id)
    {
        $skill = Skill::find($id);
        return view('dashboard.skill.show', compact('skill'));
    }

    public function edit($id)
    {
        $skill = Skill::find($id);
        return view('dashboard.skill.edit', compact('skill'));
    }
    public function update(Request $request, $id)
    {
        $skill = Skill::find($id);
        $skill->name = $request->name;
        $skill->status = $request->status;
        $skill->update();
        return redirect('/skills')->with(['success' => 'Project has been updated.']);
    }

    public function destroy($id)
    {
        Skill::find($id)->delete();
        return redirect('/skills')->with(['success' => 'Project has been Deleted.']);
    }
}
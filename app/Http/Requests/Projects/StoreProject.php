<?php

namespace App\Http\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

class StoreProject extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'visit' => 'required',
            'gitlab' => 'required',
            'description' => 'required',
            'status' => 'required',
        ];
    }
}